function getMyEle(id) {
    return document.getElementById(id);
}

// Bài tập 1
function sapXepSoTangDan() {
    var soNguyen1 = getMyEle("so-nguyen-1").value * 1;
    var soNguyen2 = getMyEle("so-nguyen-2").value * 1;
    var soNguyen3 = getMyEle("so-nguyen-3").value * 1;
    var showKetQua = getMyEle("show-ket-qua-bt1");

    soNhoNhatGiua1va2 = soNguyen1 < soNguyen2 ? soNguyen1 : soNguyen2;
    soLonNhatGiua1vs2 = soNguyen1 + soNguyen2 - soNhoNhatGiua1va2;

    soNhoNhat = soNhoNhatGiua1va2 < soNguyen3 ? soNhoNhatGiua1va2 : soNguyen3;
    soLonNhat = soLonNhatGiua1vs2 > soNguyen3 ? soLonNhatGiua1vs2 : soNguyen3;
    soGiua = soNguyen1 + soNguyen2 + soNguyen3 - soNhoNhat - soLonNhat;

    showKetQua.style.display = "block";
    showKetQua.innerHTML = `Thứ tự tăng dần : ${soNhoNhat} ${soGiua} ${soLonNhat} `
}

// Bài tập 2
function sayHello() {
    var bo = getMyEle("bo");
    var me = getMyEle("me");
    var anhTrai = getMyEle("anh-trai");
    var emGai = getMyEle("em-gai");
    var hoTen = getMyEle("ho-ten-thanh-vien").value;
    var showKetQua = getMyEle("show-ket-qua-bt2");
    if (bo.checked) {
        showKetQua.innerHTML = `<p>Xin chào bác trai ${hoTen}</p>`
    } else if (me.checked) {
        showKetQua.innerHTML = `<p>Xin chào bác gái ${hoTen}</p>`
    } else if (emGai.checked) {
        showKetQua.innerHTML = `<p>Xin chào chị ${hoTen}</p>`
    } else {
        showKetQua.innerHTML = `<p>Xin chào anh ${hoTen}</p>`
    }
}

// Bài tập 3
function tinhSoChanLe() {
    var soNguyen1 = getMyEle("so-nguyen-1-bt3").value * 1;
    var soNguyen2 = getMyEle("so-nguyen-2-bt3").value * 1;
    var soNguyen3 = getMyEle("so-nguyen-3-bt3").value * 1;
    var showKetQua = getMyEle("show-ket-qua-bt3");
    var demSoChan = 0, demsoLe = 0;

    soNguyen1 % 2 == 0 ? ++demSoChan : ++demsoLe;
    soNguyen2 % 2 == 0 ? ++demSoChan : ++demsoLe;
    soNguyen3 % 2 == 0 ? ++demSoChan : ++demsoLe;

    showKetQua.style.display = "block";
    showKetQua.innerHTML = `Có ${demSoChan} số chẵn và ${demsoLe} số lẻ`
}

// Bài tập 4
function xetTinhChatTamGiac() {
    var canh1 = getMyEle("canh-1").value * 1;
    var canh2 = getMyEle("canh-2").value * 1;
    var canh3 = getMyEle("canh-3").value * 1;
    var showKetQua = getMyEle("show-ket-qua-bt4");

    if (canh1 == canh2 && canh1 == canh3 && canh2 == canh3) {
        showKetQua.style.display = "block";
        showKetQua.innerHTML = "Đây là tam giác đều";
    } else if (canh1 == canh2 || canh1 == canh3 || canh2 == canh3) {
        showKetQua.style.display = "block";
        showKetQua.innerHTML = "Đây là tam giác cân";
    } else if (Math.pow(canh1, 2) == Math.pow(canh2, 2) + Math.pow(canh3, 2) || Math.pow(canh2, 2) == Math.pow(canh1, 2) + Math.pow(canh3, 2) || Math.pow(canh3, 2) == Math.pow(canh1, 2) + Math.pow(canh2, 2)) {
        showKetQua.style.display = "block";
        showKetQua.innerHTML = "Đây là tam giác vuông";
    } else {
        showKetQua.style.display = "block";
        showKetQua.innerHTML = "Đây là tam giác thường";
    }
}
